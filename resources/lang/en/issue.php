<?php

return [

    'gender' => [
        'male' => 'Mr.',
        'female' => 'Ms.',
    ]

];
