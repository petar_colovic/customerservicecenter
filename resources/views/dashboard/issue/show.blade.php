@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('issue.gender.' . $issue->gender) }} {{ $issue->name }}
                        <span class="pull-right">
                            @if($issue->deleted_at)
                                <a href="{{ route('dashboard.issue.archived') }}">{{ __('Archived Issues') }}</a>
                            @else
                                <a href="{{ route('dashboard.issue.index') }}">{{ __('Active Issues') }}</a>
                            @endif
                        </span>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <b>{{ __('Created:') }}</b>
                                <p>
                                    {{ $issue->created_at }}
                                </p>
                                <b>{{ __('Callback:') }}</b>
                                <p>
                                    {{ $issue->callback }}
                                </p>
                                <b>{{ __('Phone:') }}</b>
                                <p>
                                    {{ $issue->phone }}
                                </p>
                                @if($issue->order)
                                <b>{{ __('Order:') }}</b>
                                <p>
                                    {{ $issue->order }}
                                </p>
                                @endif
                                @if($issue->email)
                                <b>{{ __('Email:') }}</b>
                                <p>
                                    {{ $issue->email }}
                                </p>
                                @endif
                                <b>{{ __('Description:') }}</b>
                                <p>
                                    {{ $issue->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <comments url="{{ route('dashboard.comment.index', $issue->id) }}"></comments>
            </div>
        </div>
    </div>
@endsection
