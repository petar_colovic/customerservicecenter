@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Archived Issues') }}
                        <span class="pull-right">
                            <a href="{{ route('dashboard.issue.index') }}">
                                {{ __('Active Issues') }}
                            </a>
                        </span>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <td>
                                    <a href="{{ route('dashboard.issue.archived', [
                                    'sort'=> 'gender',
                                    'direction' => (request()->sort == 'gender' && request()->direction == 'asc' ? 'desc' : 'asc')
                                    ]) }}">
                                        {{ __('Gender') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('dashboard.issue.archived', [
                                    'sort'=> 'name',
                                    'direction' => (request()->sort == 'name' && request()->direction == 'asc' ? 'desc' : 'asc')
                                    ]) }}">
                                        {{ __('Name') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('dashboard.issue.archived', [
                                    'sort'=> 'callback',
                                    'direction' => (request()->sort == 'callback' && request()->direction == 'asc' ? 'desc' : 'asc')
                                    ]) }}">
                                        {{ __('Callback') }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('dashboard.issue.archived', [
                                    'sort'=> 'created_at',
                                    'direction' => (request()->sort == 'created_at' && request()->direction == 'asc' ? 'desc' : 'asc')
                                    ]) }}">
                                        {{ __('Created') }}
                                    </a>
                                </td>
                                <td>{{ __('Actions') }}</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($issues as $issue)
                            <tr>
                                <td>
                                    {{ __('issue.gender.' . $issue->gender) }}
                                </td>
                                <td>{{ $issue->name }}</td>
                                <td>{{ $issue->callback->format('d.m.Y H:i') }}</td>
                                <td>{{ $issue->created_at->format('d.m.Y H:i') }}</td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <form action="{{ route('dashboard.issue.restore', $issue->id) }}" method="POST" onsubmit="return confirm('{{ __('Are you sure?') }}');">
                                            @method('patch')
                                            @csrf
                                            <a href="{{ route('dashboard.issue.show', $issue->id) }}" class="btn btn-primary">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <button class="btn btn-secondary"><i class="fa fa-undo"></i></button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="container">
                        <div class="row">
                            <div class="mx-auto">
                                {{ $issues->appends(request()->all())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
