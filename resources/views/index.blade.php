@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Customer Service Center</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>
                            Welcome to our support center. Please do not hesitate to report what ever issue you may have.
                        </p>

                        <p>
                            <a href="{{ route('issues.create') }}">{{ __('Create an issue') }}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
