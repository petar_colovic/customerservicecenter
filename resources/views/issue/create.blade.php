@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Report an issue') }}</div>

                    <div class="card-body">
                        <form action="{{ route('issues.store') }}" method="post" novalidate>
                            @csrf
                            <div class="form-group">
                                <label for="gender">{{ __('Gender') }}</label>
                                <select class="form-control @error('gender') is-invalid @enderror" id="gender" name="gender" required>
                                    <option value="male" {{ old('gender') == 'male' ? 'selected="selected"' : '' }}>{{ __('issue.gender.male') }}</option>
                                    <option value="female" {{ old('gender') == 'female' ? 'selected="selected"' : '' }}>{{ __('issue.gender.female') }}</option>
                                </select>
                                @error('gender')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="name">{{ __('Name') }}</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="{{ __('Name') }}" value="{{ old('name') }}" required>
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="phone">{{ __('Phone') }}</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" placeholder="{{ __('Phone') }}" value="{{ old('phone') }}" required>
                                @error('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="order">{{ __('Order') }}</label>
                                <input type="text" class="form-control @error('order') is-invalid @enderror" id="order" name="order" placeholder="{{ __('Order') }}" value="{{ old('order') }}">
                                @error('order')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('Email') }}</label>
                                <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="{{ __('Email') }}" value="{{ old('email') }}">
                                @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">{{ __('Description') }}</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="{{ __('Description') }}" required>{{ old('description') }}</textarea>
                                @error('description')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="callback">{{ __('Callback') }}</label>
                                <div class="input-group date" id="callback" data-target-input="nearest">
                                    <input type="text" class="form-control @error('callback') is-invalid @enderror" id="callback" name="callback" placeholder="{{ __('Callback') }}" value="{{ old('callback') }}" required>
                                    <div class="input-group-append" data-target="#callback" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    @error('callback')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input @error('terms') is-invalid @enderror" type="checkbox" name="terms" value="1" id="terms" {{ old('terms') ? 'checked="checked"' : '' }} required>
                                    <label class="form-check-label" for="terms">
                                        {{ __('Agree to terms and conditions') }}
                                    </label>
                                    @error('terms')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">{{ __('Submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
