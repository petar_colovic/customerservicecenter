# Makefile for Local environment

env ?= .docker/.env
include $(env)
export $(shell sed 's/=.*//' $(env))

.EXPORT_ALL_VARIABLES:
UID=$(shell echo $$(id -u))
GID=$(shell echo $$(id -g))

.DEFAULT_GOAL := help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

ssl: ## Generate SSL keys
	@mkdir .docker/ssl
	@openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout .docker/ssl/server.key -out .docker/ssl/server.crt

up: ## Start docker
	@docker-compose up -d

stop: ## Stop docker
	@docker-compose stop

down: ## Down docker
	@docker-compose down

rm: ## Remove docker
	@docker-compose rm

build: stop rm ## Rebuild docker
	@docker-compose build

rebuild: stop rm ## Start and build docker
	@docker-compose up --force-recreate --build

composer-update: ## Composer update
	@docker-compose exec php composer update

composer-install: ## Composer install
	@docker-compose exec php composer install

composer-require: ## Composer require
	@docker-compose exec php composer require ${ARGS}

art: ## Artisan command
	@docker-compose exec php php artisan ${ARGS}

first-install: ## Run docker, execute composer & artisan commands
	@make ssl
	@make up
	@make composer-install
	@sleep 10 # Waiting for DB
	@make art ARGS="migrate:fresh --seed"
	@echo "All done, add to /etc/hosts domain https://tms.test/ or use https://localhost/"
