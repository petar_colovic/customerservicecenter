<?php

namespace App\Http\Controllers\Dashboard;

use App\Issue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class IssueController extends Controller
{
    const PER_PAGE = 10;

    /**
     * List issues.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $issues = Issue::sort($request->get('sort'), $request->get('direction'))->paginate(self::PER_PAGE);

        return view('dashboard.issue.index', ['issues' => $issues]);
    }

    /**
     * Archived issues.
     *
     * @param Request $request
     * @return Response
     */
    public function archived(Request $request)
    {
        $issues = Issue::onlyTrashed()->sort($request->get('sort'), $request->get('direction'))->paginate(self::PER_PAGE);

        return view('dashboard.issue.archived', ['issues' => $issues]);
    }

    /**
     * Show issue.
     *
     * @param $issue
     * @return Response
     */
    public function show($issue)
    {
        $issue = Issue::withTrashed()->findOrFail($issue);

        return view('dashboard.issue.show', ['issue' => $issue]);
    }

    /**
     * Destroy issue.
     *
     * @param Issue $issue
     * @return Response
     * @throws \Exception
     */
    public function destroy(Issue $issue)
    {
        $issue->delete();

        return redirect()->back()->with('status', __('Successfully created issue'));
    }

    /**
     * Restore issue.
     *
     * @param $issue
     * @return Response
     */
    public function restore($issue)
    {
        Issue::onlyTrashed()->findOrFail($issue)->restore();

        return redirect()->back()->with('status', __('Successfully created issue'));
    }
}
