<?php

namespace App\Http\Controllers\Dashboard;

use App\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Controllers\Controller;
use App\Issue;
use Illuminate\Http\Response;

class CommentController extends Controller
{
    const PER_PAGE = 5;

    /**
     * List comments.
     *
     * @param $issue
     * @return Response
     */
    public function index($issue)
    {
        return Comment::byIssue($issue)->paginate(self::PER_PAGE);
    }

    /**
     * Store comment.
     *
     * @param StoreCommentRequest $request
     * @param string $issue
     * @return Comment
     */
    public function store($issue, StoreCommentRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();

        $comment = new Comment();
        $comment->title = $user->name;
        $comment->body = $data['body'];
        $comment->commentable_id = $issue;
        $comment->commentable_type = Issue::class;
        $comment->save();

        return $comment;
    }
}
