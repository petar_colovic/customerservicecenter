<?php

namespace App\Http\Controllers;

use App\Issue;
use App\Http\Requests\StoreIssuesRequest;
use Illuminate\Http\Response;

class IssueController extends Controller
{
    /**
     * Create issue.
     *
     * @return Response
     */
    public function create()
    {
        return view('issue.create');
    }

    /**
     * Store issue.
     *
     * @param  StoreIssuesRequest  $request
     * @return Response
     */
    public function store(StoreIssuesRequest $request)
    {
        Issue::create($request->validated());

        return redirect('/')->with('status', __('Successfully created issue'));
    }
}
