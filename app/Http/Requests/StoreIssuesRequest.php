<?php

namespace App\Http\Requests;

use App\Rules\AvailableDateTime;
use Illuminate\Foundation\Http\FormRequest;

class StoreIssuesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'gender' => 'required|in:male,female',
            'phone' => 'required',
            'order' => 'nullable|string',
            'email' => 'nullable|email',
            'description' => 'required',
            'callback' => [new AvailableDateTime()],
            'terms' => 'required_without_all',
        ];
    }
}
