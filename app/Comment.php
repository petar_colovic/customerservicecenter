<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Comment extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'commentable_id', 'commentable_type',
    ];

    /**
     * Get the commentable entity that the comment belongs to.
     *
     * @return mixed
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Issue.
     *
     * @param  Builder  $query
     * @param  string|int  $issue
     * @return Builder
     */
    public function scopeByIssue($query, $issue)
    {
        return $query
            ->where([['commentable_id', $issue], ['commentable_type', Issue::class]])
            ->orderBy('created_at', 'desc');
    }
}
