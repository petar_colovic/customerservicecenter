<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AvailableDateTime implements Rule
{
    /**
     * @var string
     */
    protected $message = '';

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validator = Validator::make([$attribute => $value], [$attribute => 'required|date_format:Y-m-d H:i']);
        if ($validator->fails()) {
            $this->message = $validator->getMessageBag()->first();

            return false;
        }

        $date = Carbon::createFromDate($value);

        $available = config('issue.hours.' . $date->dayOfWeek);
        if ($date->minute) {
            $date->hour++;
        }

        if (!in_array($date->dayOfWeek, config('issue.dayInWeek'))) {
            $this->message = __(
                'The :attribute must be one of :days Days in week',
                ['days' => implode(', ', config('issue.dayInWeek'))]
            );

            return false;
        }

        if (!in_array($date->minute, config('issue.minutes'))) {
            $this->message = __(
                'The :attribute must be in time periods of :minutes Minutes',
                ['minutes' => implode(', ', config('issue.minutes'))]
            );

            return false;
        }

        if (!($available['from'] <= $date->hour && $available['to'] >= $date->hour)) {
            $this->message = __(
                'The :attribute must be between time :hours Hours',
                ['hours' => $available['from'] . ' - ' . $available['to']]
            );

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
