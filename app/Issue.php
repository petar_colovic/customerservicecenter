<?php

namespace App;

use Carbon\Carbon;
use App\Traits\Commentable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Issue extends Model
{
    use SoftDeletes, Commentable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'gender', 'phone', 'order', 'email', 'description', 'callback', 'terms'
    ];

    protected $sortable = [
        'name', 'gender', 'callback', 'created_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'callback' => 'datetime',
        'terms' => 'boolean',
    ];

    /**
     * Set the callback date.
     *
     * @param  string|array  $value
     * @return void
     */
    public function setCallbackAttribute($value)
    {
        if (is_string($value)) {
            $value = Carbon::createFromTimeString($value);
        }

        $this->attributes['callback'] = $this->castAttribute('callback', $value);
    }

    /**
     * Sort.
     *
     * @param  Builder  $query
     * @param  string  $order
     * @param  string  $direction
     * @return Builder
     */
    public function scopeSort($query, $order, $direction)
    {
        if ($order && in_array($order, $this->sortable)) {
            return $query->orderBy($order, $direction ?: 'asc');
        }

        return $query->orderBy('created_at', 'desc');
    }
}
