<?php

namespace App\Traits;

use App\Comment;

trait Commentable
{
    /**
     * Get the commentable entity that the comment belongs to.
     *
     * @return mixed
     */
    public function comment()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
