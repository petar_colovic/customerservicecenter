### Development environment
Local environment is set with docker, you can use it with make commands:

    # Create self-signed cert
    make ssl

    # Start / Stop docker
    make up
    make stop

    # Composer run in docker
    make composer-update
    make composer-install

    # Artisan commands
    make art ARGS="migrate"
    make art ARGS="db:seed"

Initializing project should be done by:

    # Docker will use ports: 80, 443, 3306
    make first-install

Any local environment can run the project:

    # Copy and adjust ENV for DB credentials of the local database
    cp .env.example .env
    # Set DocumentRoot to /public
    composer install
    php artisan migrate:fresh --seed
    

Admin User:

    admin@admin.test
    password
