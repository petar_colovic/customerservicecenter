<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index');

Route::get('/issues/create', 'IssueController@create')->name('issues.create');
Route::post('/issues', 'IssueController@store')->name('issues.store');

Auth::routes([
    'register' => false,
    'confirm' => false,
    'verify' => false,
]);

Route::redirect('/dashboard', '/dashboard/issues');
Route::prefix('dashboard')->middleware('auth')->group(function () {
    // Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/issues', 'Dashboard\IssueController@index')->name('dashboard.issue.index');
    Route::get('/issues/archived', 'Dashboard\IssueController@archived')->name('dashboard.issue.archived');
    Route::get('/issues/{issue}', 'Dashboard\IssueController@show')->name('dashboard.issue.show');
    Route::delete('/issues/{issue}', 'Dashboard\IssueController@destroy')->name('dashboard.issue.destroy');
    Route::patch('/issues/{issue}', 'Dashboard\IssueController@restore')->name('dashboard.issue.restore');
    Route::get('/issues/{issue}/comments', 'Dashboard\CommentController@index')->name('dashboard.comment.index');
    Route::post('/issues/{issue}/comments', 'Dashboard\CommentController@store')->name('dashboard.comment.store');
});
