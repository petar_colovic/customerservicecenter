<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Issue;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Issue::class, function (Faker $faker) {
    $genders = ['male', 'female'];

    return [
        'name' => $faker->name,
        'gender' => $genders[array_rand($genders)],
        'phone' => $faker->phoneNumber,
        'order' => $faker->buildingNumber,
        'email' => $faker->unique()->safeEmail,
        'description' => $faker->paragraph,
        'callback' => $faker->dateTimeThisYear,
        'terms' => $faker->boolean,
        'deleted_at' => $faker->boolean ? now() : null,
    ];
});
