<?php

use Illuminate\Database\Seeder;

class IssuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Issue::class, 100)->create()->each(function ($issue) {
            $issue->comment()->createMany(factory(App\Comment::class, rand(1, 10))->raw());
        });
    }
}
